const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const api = require('./app/routes/turorial.routes');

const PORT = process.env.PORT || 8081;
const app = express();

const db = require('./app/models/connection');
db.sequelize.sync();

// In development, you may need to drop existing tables and re-sync database. Just use force: true as following code:
// db.sequelize.sync({ force: true }).then(() => {
//   console.log('Drop and re-sync db.');
// });

const corsOptions = {
  origin: 'http://localhost:8081',
};
app.use(cors(corsOptions));

app.listen(PORT, function () {
  console.log(`Server is listening @ http://localhost:${PORT}`);
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.json({ message: 'Welcome to mysql.' });
});

app.use('/api/tutorials', api);

//https://bezkoder.com/node-js-express-sequelize-mysql/
