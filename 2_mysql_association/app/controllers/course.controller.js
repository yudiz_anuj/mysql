const db = require('../models/index');
const Students = db.Students;
const Courses = db.Courses;
const Op = db.Sequelize.Op;

// Create a new object
// Create and Save a new Tutorial:
exports.create = async (req, res) => {
  // Validate request
  if (!req.body.c_id) {
    res.status(400).send({
      message: 'Content can not be empty!',
    });
    return;
  }
  // Create a Tutorial
  const course = {
    c_id: req.body.c_id,
    name: req.body.name,
    credit: req.body.credit,
    s_id: req.body.s_id,
  };

  // Save Tutorial in the database
  await Courses.create(course)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while creating the Tutorial.',
      });
    });
};

// Create and Save Multiple Course
//course comes as  array of objects in request
exports.createBulk = async (req, res) => {
  await Courses.bulkCreate(req.body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while creating the Tutorial.',
      });
    });
};
// assign student to course
exports.assignStudent = async (req, res) => {
  try {
    const { c_id, s_id } = req.body;
    const course = await Courses.findOne({ where: { c_id: c_id } });
    const student = await Students.findOne({ where: { s_id: s_id } });
    const juinctionData = await course.addStudent(student);
    console.log(juinctionData);
    res.json({ course, student });
  } catch (err) {
    res.status(500).send({
      message:
        err.message || 'Some error occurred while creating the Tutorial.',
    });
  }
};

//testAssociation
exports.test_association = async (req, res) => {
  try {
    const data = await Courses.findAll({
      include: [Students],
      // where: {},
      // group: 'age',
    });

    res.send(data);
  } catch (err) {
    res.send(err);
  }
};
