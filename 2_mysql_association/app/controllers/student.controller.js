const db = require('../models/index');
const Students = db.Students;
const Courses = db.Courses;
const Op = db.Sequelize.Op;

// Create a new object
// Create and Save a new Tutorial:
exports.create = async (req, res) => {
  // Validate request
  if (!req.body.name) {
    res.status(400).send({
      message: 'Content can not be empty!',
    });
    return;
  }
  // Create a student
  const new_student = {
    name: req.body.name,
    age: req.body.age,
    c_id: req.body.c_id,
  };
  // Save Student in the database
  try {
    const student = await Students.create(new_student);
    // console.log(await student.getCourses());
    res.send(student);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || 'Some error occurred while creating the Tutorial.',
    });
  }
};

// Create and Save Multiple Student
//Student comes as  array of objects in request
exports.createBulk = async (req, res) => {
  await Students.bulkCreate(req.body)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while creating the Tutorial.',
      });
    });
};
// assign Course to student
exports.assignCourse = async (req, res) => {
  try {
    const { c_id, s_id } = req.body;
    const course = await Courses.findOne({ where: { c_id: c_id } });
    const student = await Students.findOne({ where: { s_id: s_id } });
    const juinctionData = await student.addCourse(course);
    console.log(juinctionData);
    res.json({ course, student });
  } catch (err) {
    res.status(500).send({
      message:
        err.message || 'Some error occurred while creating the Tutorial.',
    });
  }
};

//testAssociation
exports.test_association = async (req, res) => {
  try {
    const data = await Students.findAll({
      include: [Courses],
      // where: {},
      // group: 'age',
    });

    res.send(data);
  } catch (err) {
    res.send(err);
  }
};
