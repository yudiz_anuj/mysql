module.exports = (sequelize, Sequelize) => {
  const Course = sequelize.define(
    'Course',
    {
      c_id: {
        type: Sequelize.CHAR(5),
        primaryKey: true,
      },
      name: {
        type: Sequelize.STRING,
      },
      credit: {
        type: Sequelize.INTEGER,
      },
    },
    {
      modelName: 'Course',
      tableName: 'Course',
      timestamps: false,
    } // option
  );

  return Course;
};
