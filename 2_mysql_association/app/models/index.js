const dbConfig = require('../config/db.config');
const Sequelize = require('sequelize');

/*
  //create db if not exist
  const { HOST, USER, PASSWORD, DB, PORT } = dbConfig;
  // prettier-ignore
  const connection = await mysql.createConnection({HOST, PORT, USER, PASSWORD});
  await connection.query(`CREATE DATABASE IF NOT EXISTS \`${DB}\`;`);
  */
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,
  logging: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle,
  },
});

const Students = require('./student.model')(sequelize, Sequelize);
const Courses = require('./course.model')(sequelize, Sequelize);

/* one-to-one => hasOne() + belongsTo() */
// Students.hasOne(Courses, {
//   constraints: true,
//   targetKey: 's_id',
//   foreignKey: { name: 's_id', allowNull: false }, // name of the foreign key column
//   onUpdate: 'CASCADE',
//   onDelete: 'CASCADE',
// });
// Courses.belongsTo(Students, {
//   constraints: true,
//   targetKey: 's_id', //
//   foreignKey: { name: 's_id', allowNull: false }, // name of the foreign key column
//   onUpdate: 'CASCADE',
//   onDelete: 'CASCADE',
// });

/* one-to-Many => hasMany() + belongsTo() */
// Students.hasMany(Courses, {
//   constraints: true,
//   targetKey: 's_id',
//   foreignKey: { name: 's_id', allowNull: false }, // name of the foreign key column
//   onUpdate: 'CASCADE',
//   onDelete: 'CASCADE',
// });
// Courses.belongsTo(Students, {
//   constraints: true,
//   targetKey: 's_id', //
//   foreignKey: { name: 's_id', allowNull: false }, // name of the foreign key column
//   onUpdate: 'CASCADE',
//   onDelete: 'CASCADE',
// });

/* Many-to-Many = >  belongsToMany + belongsToMany */
Courses.belongsToMany(Students, {
  through: 'StudentCourse',
  foreignKey: { name: 'c_id' }, // name of the foreign key column
  onUpdate: 'CASCADE',
  onDelete: 'CASCADE',
  timestamps: false,
});
Students.belongsToMany(Courses, {
  through: 'StudentCourse',
  foreignKey: { name: 's_id' }, // name of the foreign key column
  onUpdate: 'CASCADE',
  onDelete: 'CASCADE',
  timestamps: false,
});

const db = {}; // dont put any thing on db {} directly
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.Students = Students;
db.Courses = Courses;

sequelize.sync();

module.exports = db;
