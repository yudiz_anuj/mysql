module.exports = (sequelize, Sequelize) => {
  const Student = sequelize.define(
    'Student',
    {
      s_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
      },
      age: {
        type: Sequelize.INTEGER,
      },
      /* this can be done directly using association function, hence not needed explicitly */
      // c_id: {
      //   type: Sequelize.CHAR(5),
      //   references: {
      //     // This is a reference to another model
      //     model: require('./course.model')(sequelize, Sequelize),
      //     // This is the column name of the referenced model
      //     key: 'c_id',
      //   },
      //   onDelete: 'CASCADE',
      //   onDelete: 'SET NULL',
      // },
    },
    {
      modelName: 'Student',
      tableName: 'Student',
      timestamps: false,
    } // option
  );

  return Student;
};
