const router = require('express').Router();

const Courses = require('../controllers/course.controller');

// Create a new course
router.post('/', Courses.create);

// Create multiple Course
router.post('/bulk', Courses.createBulk);

//assign student to course
router.post('/assign_student', Courses.assignStudent);

// Test Association
router.get('/test_association', Courses.test_association);

module.exports = router;
