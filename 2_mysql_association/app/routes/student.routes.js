const router = require('express').Router();

const Students = require('../controllers/student.controller');

// Add a new student
router.post('/', Students.create);

// Add Multiple Students
router.post('/bulk', Students.createBulk);

//assign course
router.post('/assign_course', Students.assignCourse);

// Test Association
router.get('/test_association', Students.test_association);

module.exports = router;
