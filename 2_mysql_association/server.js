const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const course_api = require('./app/routes/course.routes');
const student_api = require('./app/routes/student.routes');

const PORT = process.env.PORT || 8080;
const app = express();

// const db = require('./app/models/index');
// db.sequelize.sync();

const corsOptions = {
  origin: 'http://localhost:8081',
};
app.use(cors(corsOptions));

app.listen(PORT, function () {
  console.log(`Server is listening @ http://localhost:${PORT}`);
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.json({ message: 'Welcome to mysql.' });
});

app.use('/api/courses', course_api);
app.use('/api/students', student_api);
