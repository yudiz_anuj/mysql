const db = require('../models/index');
const Accounts = db.Accounts;
const Users = db.Users;
const Op = db.Sequelize.Op;

exports.create = async (req, res) => {
  if (!req.body.ac_number)
    return res.status(400).send({ message: 'Content can not be empty!' });

  const account = {
    ac_number: req.body.ac_number,
    balance: req.body.balance,
    u_id: req.body.u_id,
  };

  try {
    const data = await Accounts.create(account);
    res.send(data);
  } catch (err) {
    res.status(500).send({
      message: err.message || 'Some error occurred while creating the Account.',
    });
  }
};

exports.createBulk = async (req, res) => {
  try {
    const data = await Accounts.bulkCreate(req.body);
    res.send(data);
  } catch (err) {
    res.status(500).send({
      message: err.message || 'Some error occurred while creating the Account.',
    });
  }
};

exports.test_association = async (req, res) => {
  try {
    const data = await Accounts.findAll({
      include: [Users],
    });
    res.send(data);
  } catch (err) {
    res.send(err);
  }
};

/* unmanaged transaction: manual rollback commit*/
exports.withdraw_fund = async (req, res) => {
  const t = await db.sequelize.transaction();
  try {
    const account = await Accounts.findOne({
      where: { ac_number: req.body.ac_number },
      transaction: t,
    });

    const data = await account.decrement(
      {
        balance: req.body.amount,
      },
      { transaction: t }
    );

    if (data.balance - req.body.amount < 0) {
      throw new Error('Insufficient balance!!!');
    }

    // If the execution reaches this line, no errors were thrown.
    // We commit the transaction.
    await t.commit();
    res.json({
      messsage: 'Withdrawl success',
      balance: data.balance - req.body.amount,
    });
  } catch (err) {
    // If the execution reaches this line, an error was thrown.
    // We rollback the transaction.
    await t.rollback(err);
    res.send({ Error: err.message });
  }
};

///*managed transaction: auto roll back commit*/
// exports.withdraw_fund = async (req, res) => {
// try {
//   const result = await db.sequelize.transaction(async (t) => {
//     const account = await Accounts.findOne({
//       where: { ac_number: req.body.ac_number },
//       transaction: t,
//     });

//     const data = await account.decrement(
//       {
//         balance: req.body.amount,
//       },
//       { transaction: t }
//     );

//     if (data.balance - req.body.amount < 0) {
//       throw new Error('Insufficient balance!!!');
//     }
//   });
//   res.send('Withdrawl success');
// } catch (err) {
//   res.send({ Error: err.message });
// }
// };
