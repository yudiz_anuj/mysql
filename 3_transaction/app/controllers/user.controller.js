const db = require('../models/index');
const Accounts = db.Accounts;
const Users = db.Users;
const Op = db.Sequelize.Op;

exports.create = async (req, res) => {
  if (!req.body.name)
    return res.status(400).send({ message: 'name can not be empty!' });

  const user = {
    name: req.body.name,
  };

  try {
    const data = await Users.create(user);
    res.send(data);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || 'Some error occurred while creating the Tutorial.',
    });
  }
};

exports.createBulk = async (req, res) => {
  try {
    const data = await Users.bulkCreate(req.body);
    res.send(data);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || 'Some error occurred while creating the Tutorial.',
    });
  }
};

exports.test_association = async (req, res) => {
  try {
    const data = await Users.findAll({
      include: [Accounts],
    });
    res.send(data);
  } catch (err) {
    res.send(err);
  }
};
