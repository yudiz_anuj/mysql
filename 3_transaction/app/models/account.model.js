module.exports = (sequelize, Sequelize) => {
  const Accounts = sequelize.define(
    'Accounts',
    {
      ac_number: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      balance: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
        allowNull: true,
      },
    },
    {
      modelName: 'Accounts',
      tableName: 'Accounts',
      timestamps: false,
    } // option
  );

  return Accounts;
};
