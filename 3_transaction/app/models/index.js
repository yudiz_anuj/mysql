const dbConfig = require('../config/db.config');
const Sequelize = require('sequelize');
const mysql = require('mysql2/promise');
(async () => {
  const connection = await mysql.createConnection({
    host: dbConfig.HOST,
    port: dbConfig.PORT,
    user: dbConfig.USER,
    password: dbConfig.PASSWORD,
  });
  try {
    await connection.query(`CREATE DATABASE IF NOT EXISTS ${dbConfig.DB};`);
    console.info('Database create or successfully checked');
    // process.exit(0);
  } catch (err) {
    console.info(`Error Occured : ${err.message}`);
  }
})();

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,
  logging: true,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle,
  },
});

const Users = require('./user.model')(sequelize, Sequelize);
const Accounts = require('./account.model')(sequelize, Sequelize);

/* one-to-one => hasOne() + belongsTo() */
Users.hasOne(Accounts, {
  constraints: true,
  targetKey: 'u_id',
  foreignKey: { name: 'u_id', allowNull: true }, // name of the foreign key column
  onUpdate: 'CASCADE',
  onDelete: 'SET NULL',
});
Accounts.belongsTo(Users, {
  constraints: true,
  targetKey: 'u_id', //
  foreignKey: { name: 'u_id', allowNull: true }, // name of the foreign key column
  onUpdate: 'CASCADE',
  onDelete: 'SET NULL',
});

const db = {}; // dont put any thing on db {} directly
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.Users = Users;
db.Accounts = Accounts;

sequelize.sync({ force: false }); //force: true => delete and recreate if already exist

module.exports = db;
