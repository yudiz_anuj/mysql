module.exports = (sequelize, Sequelize) => {
  const Users = sequelize.define(
    'Users',
    {
      u_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(20),
        allowNull: false,
      },
    },
    {
      modelName: 'Users',
      tableName: 'Users',
      timestamps: false,
    } // option
  );
  return Users;
};
