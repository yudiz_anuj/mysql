const router = require('express').Router();

const Accounts = require('../controllers/account.controller');

// Add a new account
router.post('/', Accounts.create);

// Add Multiple accounts
router.post('/bulk', Accounts.createBulk);

// get accounts with their users
router.get('/test_association', Accounts.test_association);

// withdraw fund
router.put('/withdraw', Accounts.withdraw_fund);

module.exports = router;
