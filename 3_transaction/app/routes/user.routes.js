const router = require('express').Router();

const Users = require('../controllers/user.controller');

// Create a new User
router.post('/', Users.create);

// Create multiple Users
router.post('/bulk', Users.createBulk);

// get users with their accounts
router.get('/test_association', Users.test_association);

module.exports = router;
