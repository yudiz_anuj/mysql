'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.renameColumn('Users', 'sMiddleName', 'aAge');
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.renameColumn('Users', 'aAge', 'sMiddleName');
  },
};
