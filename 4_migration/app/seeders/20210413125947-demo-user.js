'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /** 
  * Add seed commands here. 
  * 
  * Example: 
  * await queryInterface.bulkInsert('People', [{ 
  * name: 'John Doe', 
  * isBetaMember: false 
  * }], {}); 

  */
    return queryInterface.bulkInsert('Users', [
      {
        sFirstName: 'Anuj',
        sLastName: 'Jha',
        sEmail: 'anuj.j@yudiz.in',
      },
      {
        sFirstName: 'Bipin',
        sLastName: '',
        sEmail: 'anuj.j@yudiz.in',
      },
      {
        sFirstName: 'Ravi',
        sLastName: '',
        sEmail: 'anuj.j@yudiz.in',
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

    return queryInterface.bulkDelete('Users', null, {});
  },
};
